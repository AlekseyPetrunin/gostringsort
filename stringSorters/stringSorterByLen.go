package stringSorters

type StringSorterByLen []string

func (s StringSorterByLen) Len() int {
	return len(s)
}

func (s StringSorterByLen) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}

func (s StringSorterByLen) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

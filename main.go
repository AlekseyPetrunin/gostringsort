package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"

	"bitbucket.com/AlekseyPetrunin/newFilter/stringSorters"
)

func readFile(path string) string {
	fileData, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	return string(fileData)
}

func main() {
	data := readFile("filterData")
	var rows []string
	rows = strings.Split(data, "\n")
	sort.Sort(stringSorters.StringSorterByLen(rows))

	for i := 0; i < len(rows); i++ {
		fmt.Println(rows[i])
	}
}
